package edabit4;

public class Ejercicio3 {

      public static int findNaN(double[] n) {
			for(int i = 0; i < n.length; i++) {
					if(Double.isNaN(n[i])) {
							return i;
					}
			}
  		return -1;
  }
}
