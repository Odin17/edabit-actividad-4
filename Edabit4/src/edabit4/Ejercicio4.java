package edabit4;

public class Ejercicio4 {

  public static String jayAndBob(String w) {
		if(w.equals("half")){
			return "14 grams";
		}
		else if(w.equals("quarter")){
			return "7 grams";
		}
		else if(w.equals("eighth")){
			return "3.5 grams";
		}
		else if(w.equals("sixteenth")){
			return "1.75 grams";
		}
		return "";
  }
}
   
